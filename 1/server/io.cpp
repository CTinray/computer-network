#include "io.hpp"
#include "error.h"
#include <cstring>

IO::IO(){
    this -> fd = -1;
}


IO::IO(int fd, IOType ioType, char* buffer, int bufferSize
       ,std::function<void (int)>onFinish, std::function<void (int)>onDestroy ){
    this -> ioType = ioType;
    this -> fd = fd;
    this -> buffer = buffer;        
    this -> bufferSize = bufferSize;
    this -> bufferStart = buffer;
    this -> onFinish = onFinish;
    this -> onDestroy = onDestroy;
}

void IO::Destroy(int errorno){
#ifdef DEBUG
    fprintf( stderr, "Close fd = %d ", fd );
#endif
    close( this -> fd );
    int tmp = fd; 
    this -> fd = -1;    
    ((this -> onDestroy))( tmp );
}

void IO::read(fd_set*readFDSet){
    int nRead = ::read( this -> fd, this -> bufferStart,
                      this -> buffer + this -> bufferSize - this -> bufferStart );
    if( nRead < 0 ){
        char strWarn[100];
        sprintf( strWarn, "Warning: fail to read on fd %d\n", this -> fd );
        warn( strWarn );
        FD_CLR(fd, readFDSet);
        Destroy(-1);
        return;
    }
    
#ifdef DEBUG
    fprintf( stderr, "Chunk receive: %s\n", this -> bufferStart );
#endif

    if( strstr( this -> buffer, "\n" ) != NULL ){
        FD_CLR( fd, readFDSet );
        ((this -> onFinish))(this -> fd);
    }else{
        this -> bufferStart += nRead;
    }
}


void IO::write(fd_set*writeFDSet){
#ifdef DEBUG
    fprintf( stderr, "Start to write on file descriptor %d\n", fd );
#endif

    int written = ::write( this -> fd, this -> bufferStart, strlen( this -> bufferStart ) );

    
#ifdef DEBUG
    fprintf( stderr, "Chunk wrote: %s\n", this -> bufferStart );
#endif

    if( written < 0 ){
        FD_CLR(fd, writeFDSet);
        Destroy(-1);
        return;
    }
    
    if( written == strlen( this -> bufferStart ) ){
        FD_CLR( fd, writeFDSet );
        ((this -> onFinish))(this -> fd);
    }
    else{
        this -> bufferStart += written;
    }

}


bool IO::IsValid(){
    if( this -> fd == -1 ){
        return false;
    }
    else{
        return true;
    }
}
