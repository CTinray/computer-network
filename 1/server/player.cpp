#include "player.hpp"

Player::Player(Server&sv, int fd, int cash ): server(sv){
    this -> fd = fd;
    this -> cash = cash;
    this -> errorCallback = [this](int fd){
        fprintf( stderr, "Error: Player with fd = %d has error with game server. (unhandle)", fd );
    };
}

void Player::OnError(std::function<void (int)>cb){
    this -> errorCallback = cb;
}

void Player::Send(std::function<void ()>finishCallback){
    server.write( this -> fd, this -> buffer, BUFFER_SIZE,
                  [finishCallback](int){ finishCallback(); }, [this](int fd){
                      errorCallback(fd);
                      delete this;
                  } );
}
    
void Player::Receive(std::function<void ()>finishCallback){
    server.read( this -> fd, this -> buffer, BUFFER_SIZE,
                 [finishCallback](int){ finishCallback(); }, [this](int fd){
                     errorCallback(fd);
                     delete this;
                 } );
}

void Player::ModifyCash(int money){
    this -> cash += money;
}


int Player::Cash(){
    return cash;
}

void Player::OnGameOver(std::function<void (Game*)>cb){
    gameOverCallback = cb;
}

void Player::GameOver(Game*game){
    gameOverCallback(game);
}

int Player::GetCash(){
    return cash;
}

void Player::Destroy(){
    server.close( this -> fd );
}
