#ifndef SERVER_HPP
#include "server.hpp"
#endif
// #include "black_jack.hpp"
#include "error.h"
#include <functional>
#include "game_system.hpp"


int main(int argc, char** argv){
    int port = 5555;
    if( argc > 1 ){
        sscanf( argv[1], "%d", &port );
    }
    
    Server server( port );
    GameSystem gameSystem( server );    
    server.onAccept([&gameSystem](int fd){            
            gameSystem.NewClient(fd);
        });
    
    server.StartIOLoop();
    return 0;
}
        
    
