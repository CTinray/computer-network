#include <stdio.h>
#include <map>
#include <vector>
#include "client.hpp"
#include <iostream>
#include <string.h>
#include "guess_number.hpp"
#include "black_jack.hpp"

// #ifndef CSTRING_HPP
// #include "cstring.hpp"
// #endif 

void GetRoomStatus(Client&client, std::map<int,int>&gnRooms,std::map<int,int>&bjRooms ){    
    char*res = client.ReadLine();
    char tmp[100];
    int nRead;
    sscanf( res, "%s%n", tmp, &nRead );
    res += nRead;
    for( int i = 0 ; i < 10 ; ++i ){
        int roomId, status;
        sscanf( res, "%d %d |%n", &roomId, &status, &nRead );
        res += nRead;
        gnRooms[roomId] = status;
    }
    for( int i = 0 ; i < 10 ; ++i ){
        int roomId, status;
        sscanf( res, "%d %d |%n", &roomId, &status, &nRead );
        res += nRead;
        bjRooms[roomId] = status;
    }
}

void PrintRoomStatus(std::map<int, int>rooms){
    std::cout << "Guess Number Rooms:" << std::endl;
    for( auto it = rooms.begin() ; it != rooms.end() ; ++it ){
        std::cout << "Room " << it -> first << ": ";
        if( ( it -> second ) == -1 ){
            std::cout << "FULL" << std::endl;
        }else{
            std::cout << it -> second << std::endl;
        }
    }
}

int ChooseRoom(std::map<int, int>rooms ){
    PrintRoomStatus( rooms );        
    int roomId;
    std::cout << "Which room do you want to get in?" << std::endl;
    std::cin >> roomId;
    while( rooms.count( roomId ) == 0 ){
        std::cout << "This room doesn't exist..., choose again." << std::endl;
        std::cin >> roomId;
    }
    return roomId;
}

int main(int argc, char**argv){
    int cash  = 10000;
    if( argc != 3 ){
        std::cerr << "Usage: client IP port" << std::endl;
        return 1;
    }
    Client client = Client(argv[1], atoi(argv[2]));


    while( true ){
        std::cout << "\033[2J" << std::endl;
        std::cout << "\033[1;1H" << std::endl;

        std::map<int, int>gnRooms, bjRooms;
        GetRoomStatus( client, gnRooms, bjRooms );
        std::cout << "You have \e[93m$" << cash << "\e[39m now!!!!!" << std::endl;
        std::cout << "What game do you want to play?" << std::endl
                  << "(1) Guess number single player mode." << std::endl
                  << "(2) Black Jack single player mode." << std::endl
                  << "(3) Guess number multi-player mode." << std::endl
                  << "(4) Black Jack multi-player mode." << std::endl;
        int gameChoise;
        std::cin >> gameChoise;
        switch( gameChoise ){
        case 1:
            cash += GuessNumber(client);
            break;
        case 2:
            cash += BlackJack(client);
            break;
        case 3:{
            int roomId = ChooseRoom( gnRooms );
            cash += GuessNumber( client, roomId );
            break;
        }
        case 4:{
            int roomId = ChooseRoom( bjRooms );
            cash += BlackJack( client, roomId );
            break;
        }
        }
    }
}
