#include <functional>
#include <unistd.h>
#define IO_HPP

class IO{
public:
    enum IOType { Read, Write };
    IO();
    IO( int fd, IOType ioType, char* buffer, int bufferSize,
        std::function<void (int)>onFinish, std::function<void (int)>onDestroy );
    void Destroy(int);
    bool IsValid();
    friend class Server;
private:
    IOType ioType;
    char* buffer, *bufferStart;
    int bufferSize;
    std::function<void (int)>onFinish;
    std::function<void (int)>onDestroy;
    void read(fd_set*);
    void write(fd_set*);
    int fd;
};
