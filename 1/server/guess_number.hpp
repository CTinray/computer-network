#include <functional>
#ifndef GAME_HPP
#include "game.hpp"
#endif
#ifndef SERVER_HPP
#include "server.hpp"
#endif
#include <vector>
#define MAX_ROUNDS 10

class GuessNumber: public Game{
public:
    GuessNumber(std::function<void (Game*)>);
    GuessNumber(Player*, std::function<void (Game*)> );
    virtual Game::Type GetType(){ return Game::GuessNumber; };
    virtual int GetId(){ return id; };
    virtual int AddPlayer(Player*);
    virtual ~GuessNumber();
private:
    int id;
    static int counter;
    void GenerateNums();
    Server*server;
    int round;
    int nums[4];
    char buffer[256];
    int bufferSize;
    int fd;
    std::function<void (Game*)>destroyCallback;
    void WaitReady(Player*);
    void ReadGuess(int);
    void JudgeGuess(int);
    void Congratulate(int);
    void TryAgain(int, int a, int b );
    void Fail(int, int a, int b );
    void Destroy();
    std::vector<Player*>players;
    std::vector<int>playerRounds;
    void send(std::function<void (int)>);
    void receive(std::function<void (int)>);
    int nPlayerReady, nPlayerFinish;
    void DispachMoney();
};

