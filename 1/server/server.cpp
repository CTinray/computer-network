#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "error.h"
#include "server.hpp"
#include <time.h>
#include <sys/types.h>
#include <signal.h>

static void HandlePipe(int sig){    
    fprintf(stderr, "SIGPIPE!\n" );    
}

Server::Server( int port){
    this -> maxFDs = getdtablesize() > MAX_FDS ? MAX_FDS : getdtablesize();
    
    // Fill servaddr
    struct sockaddr_in servaddr;
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    this -> listenFD = socket(AF_INET, SOCK_STREAM, 0);

    // int tmp = 1;
    // if (setsockopt( this -> listenFD, SOL_SOCKET, SO_REUSEADDR, (void*)&tmp, sizeof(tmp)) < 0) {
    //     error("Error setsockopt");
    // }

    
    if (bind( this -> listenFD, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
        error( "Error binding socket!!" );
    }
    if (listen(this -> listenFD, this -> maxFDs ) < 0) {
        error("Error listening");
    }

    fprintf( stdout, "Server listening on port %d\n", port );

    // Init fd_sets
    FD_ZERO( &(this -> readFDSet ) );
    FD_ZERO( &(this -> writeFDSet ) );
    FD_ZERO( &(this -> exceptionFDSet ) );

    // Empty ios
    for( int i = 0; i < MAX_FDS ; ++i ){
        this -> ios[i] = IO();
    }
    
    // Watch server socket file descripter ready to read
    FD_SET( this -> listenFD, &( this -> readFDSet) );

    struct sigaction action;
    action.sa_handler = HandlePipe;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, NULL);
}

void Server::onAccept( std::function<void (int)> callback ){
    this -> onAcceptCallback = &callback;
}

void Server::read(int fd, char*buffer, int bufferSize,
                     std::function<void (int)>onFinish, std::function<void (int)>onDestroy){
    ios[fd] = IO( fd, IO::IOType::Read, buffer, bufferSize, onFinish, onDestroy );
    FD_SET( fd, &(this -> readFDSet ) );
}

void Server::write(int fd, char*buffer, int bufferSize,
                     std::function<void (int)>onFinish, std::function<void (int)>onDestroy){
    ios[fd] = IO( fd, IO::IOType::Write, buffer, bufferSize, onFinish, onDestroy );
    FD_SET( fd, &(this -> writeFDSet ) );
}

void Server::close(int fd){
    FD_CLR( fd, &readFDSet );
    FD_CLR( fd, &readFDSetCpy );
    FD_CLR( fd, &writeFDSet );
    FD_CLR( fd, &writeFDSetCpy );    
    ios[fd].Destroy( -1 );
#ifdef DEBUG
    fprintf( stderr, "Server close one connection with fd = %d\n", fd );
#endif
}

void Server::StartIOLoop(){
    this -> enableIOLoop = true;
    while( this -> enableIOLoop ){
        readFDSetCpy = this -> readFDSet;
        writeFDSetCpy = this -> writeFDSet;
        excFDSetCpy = this -> exceptionFDSet;
        timeval timeout = { SEL_TIMEOUT, 0 };        
        select( this -> maxFDs, &readFDSetCpy, &writeFDSetCpy, &excFDSetCpy, &timeout );
        if( FD_ISSET( this -> listenFD, &readFDSetCpy ) ){
            int acceptedFD = accept( this -> listenFD, NULL, NULL );
#ifdef DEBUG
            fprintf( stderr, "Accept one connection with fd = %d\n", acceptedFD );
#endif
            (*(this -> onAcceptCallback))( acceptedFD );
            FD_CLR(  this -> listenFD, &readFDSetCpy );
        }
        for( int fd = 0 ; fd < this -> maxFDs ; ++fd ){
            if( FD_ISSET( fd, &readFDSetCpy )){
                this -> ios[fd].read( &readFDSet );
            }
            if( FD_ISSET( fd, &writeFDSetCpy ) ){
                this -> ios[fd].write( &writeFDSet );
            }
        }
        if( timeout.tv_sec == 0 ){
#ifdef DEBUG
            fprintf( stderr, "Check connections alive.\n" );
#endif
            this -> CheckIOStatus();
            timeout.tv_sec = SEL_TIMEOUT;
        }
    }
}

void Server::CheckIOStatus(){
    for( int i = 0 ; i < this -> maxFDs ; ++i ){
        if( this -> ios[i].fd != -1 ){
            int error = 0;
            socklen_t len = sizeof (error);
            ::write( ios[i].fd, " ", 1 );
            getsockopt ( ios[i].fd, SOL_SOCKET, SO_ERROR, &error, &len);
            if( error != 0 ){
#ifdef DEBUG
                fprintf( stderr, "Connection of fd = %d close.\n", ios[i].fd );
#endif
                this -> close( i );
            }
        }
    }
}
    
