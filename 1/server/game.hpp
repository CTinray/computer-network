#ifndef PLAYER_HPP
#include "player.hpp"
#endif

class Player;

class Game{    
public:
    enum Type{ GuessNumber, BlackJack, Error };
    enum Status{ Playing, Available };    
    virtual int AddPlayer(Player*) = 0;
    virtual Type GetType() = 0;
    virtual int GetId() = 0;
    int GetNPlayers();
    virtual ~Game(){};
    
protected:
    int nPlayers;
    Status status = Status::Available;
    //virtual ~Game();
};

#define GAME_HPP
    
