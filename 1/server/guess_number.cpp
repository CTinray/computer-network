#include <stdio.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "guess_number.hpp"

#include <iostream>

int GuessNumber::counter = 0;


GuessNumber::GuessNumber(std::function<void (Game*)>onDestroy)
    :destroyCallback(onDestroy){
    id = counter++;
#ifdef GAME_LOG    
    printf("Guess Number game start!! ( id = %d )\n", id);
#endif
    srand( time(NULL) );
    this -> nPlayers = 0;
    this -> nPlayerReady = 0;
    this -> nPlayerFinish = 0;
    this -> status = Status::Available;
}    


GuessNumber::GuessNumber(Player*player, std::function<void (Game*)>onDestroy)
    :GuessNumber( onDestroy )
{
    //status = Status::Playing;
    AddPlayer( player );
}

int GuessNumber::AddPlayer(Player* player){
    if( nPlayers >= 4 || status == Status::Playing){
        return -1;
    }
    int playerInd = nPlayers;
    
    player -> OnError( [this, playerInd](int){
#ifdef GAME_LOG
            fprintf( stderr, "Player with ind %d leave !!\n", playerInd );
#endif
            nPlayers -= 1;
            players.erase( players.begin() + playerInd );
            playerRounds.erase( playerRounds.begin() + playerInd );
            if( nPlayers == 0 ){
                Destroy();
            }
        });    
        
    this -> nPlayers += 1;
    players.push_back( player );
    playerRounds.push_back( 0 );
    this -> GenerateNums();
    strcpy( player -> buffer, "WAITING\n" );
    player -> Send( [this, player](){
            this -> WaitReady( player );
        });
    return 1;
}    

void GuessNumber::WaitReady(Player*player){
    player -> Receive( [this, player](){
            nPlayerReady += 1;
            if( nPlayerReady == nPlayers ){
                status = Status::Playing;
                for( int i = 0 ; i < nPlayers ; ++i ){
                    strcpy( players[i] -> buffer, "START\n" );
                    std::cerr << "This " << this << std::endl;
                    std::cerr << "players[i] " << players[i] -> buffer  << std::endl;
                    std::cerr << "i = " << i  << std::endl;
                    std::function<void (void)> cb= [this, i](){ this -> ReadGuess(i); };
                    players[i] -> Send(cb );
                }
            }
        });    
}
            

void GuessNumber::GenerateNums(){
    int numPool[] = {0, 1, 2, 3, 4,
                   5, 6, 7, 8, 9 };
    for( int i = 0 ; i < 4 ; ++i ){
        int randIndex = ( rand() % ( 10 - i ) ) + i;
        std::swap( numPool[i], numPool[randIndex] );
        nums[i] = numPool[i];
    }
#ifdef GAME_LOG
    printf( "Random number generated: %d %d %d %d\n", nums[0], nums[1], nums[2], nums[3] );
#endif 
}

void GuessNumber::ReadGuess(int playerInd){
#ifdef DEBUG
    fprintf(stderr, "Guess number waiting for input.\n");
#endif
    players[playerInd] -> Receive( [this, playerInd](){ this -> JudgeGuess(playerInd); } );
}

bool verifyGuess( int guessNums[4] ){
    int appear[10] = {0};
    for( int i = 0 ; i < 4 ; ++i ){
        if( guessNums[i] < 0 || guessNums[i] > 9 || appear[guessNums[i]] ){
            return false;
        }
        appear[guessNums[i]] = 1;
    }
    return true;
}

void GuessNumber::JudgeGuess(int playerInd){
    Player*player = players[playerInd];
    
    int guessNums[4];
    sscanf( player -> buffer, "%d %d %d %d",
            &guessNums[0], &guessNums[1], &guessNums[2], &guessNums[3] );

#ifdef GAME_LOG
    printf("Client guess %d %d %d %d. Round %d\n",
           guessNums[0], guessNums[1], guessNums[2], guessNums[3], playerRounds[playerInd] );
#endif
    
    if( !verifyGuess( guessNums ) ){
#ifdef GAME_LOG
        printf("Invalid input! Detroy player!!\n");
#endif
        //player -> Destroy();
        nPlayers -= 1;
        if( nPlayers == 0 ){
            this -> Destroy();
        }
        return;
    }

    int a = 0, b = 0;
    for( int i = 0 ; i < 4 ; ++i ){
        if( guessNums[i] == nums[i] ){
            a += 1;
            continue;
        }
        for( int j = 0 ; j < 4 ; ++j ){
            if( guessNums[i] == nums[j] ){
                b += 1;
                break;
            }
        }
    }
    
    playerRounds[playerInd] += 1;
                 
    if( a == 4 ){
        this -> Congratulate(playerInd);
    }
    else if( this -> playerRounds[playerInd] < MAX_ROUNDS ){
        this -> TryAgain(playerInd, a, b);
    }
    else{
        this -> Fail(playerInd, a, b);
    }
}
            

void GuessNumber::Congratulate(int playerInd){            
    Player*player = players[playerInd];
    sprintf(player -> buffer, "WIN\n" );
    player -> Send( [this](){
            nPlayerFinish += 1;
            if( nPlayerFinish == nPlayers ){
                DispachMoney();
            }
        } );
}

void GuessNumber::TryAgain(int playerInd, int a, int b){
    Player*player = players[playerInd];
    sprintf(player -> buffer, "TRY_AGAIN %d %d\n", a, b );
    std::cerr << "player " << playerInd << " " << player -> buffer << std::endl;
    player -> Send( [this, playerInd, player]{
            this -> ReadGuess(playerInd);
        });
}

void GuessNumber::Fail(int playerInd, int a, int b){
    sprintf(players[playerInd] -> buffer, "FAIL %d %d\n %d %d %d %d.\n",
            a, b, nums[0], nums[1], nums[2], nums[3] );
    players[playerInd] -> Send( [this](){
            nPlayerFinish += 1;
            if(nPlayerFinish == nPlayers ){
                DispachMoney();
            }
        });
}    

void GuessNumber::Destroy(){
    delete this;
}

void GuessNumber::DispachMoney(){
    if( nPlayers == 1 ){
        if( playerRounds[0] < 10 ){
            players[0] -> ModifyCash( +500 );
            sprintf( players[0] -> buffer, "CASH %d %d\n", 500, players[0] -> GetCash() );
        }
        else{
            players[0] -> ModifyCash( -500 );
            sprintf( players[0] -> buffer, "CASH %d %d\n", -500, players[0] -> GetCash() );
        }
        players[0] -> Send( [this](){
                players[0] -> GameOver((Game*)this);
            });                    
    }
    else{
        int minRound = 100;
        int minPlayerInd = 0;
        int winnerGet = 0;
        for( unsigned int i = 0 ; i < playerRounds.size() ; ++i ){
            if( playerRounds[i] < minRound ){
                minRound = playerRounds[i];
                minPlayerInd = i;
            }
        }
        for( int i = 0 ; i < nPlayers ; ++i ){
            int cashOffset = 100 * ( playerRounds[i] - minRound );
            players[i] -> ModifyCash( -cashOffset );
            players[minPlayerInd] -> ModifyCash( cashOffset );
            if( i != minPlayerInd ){
                winnerGet += cashOffset;            
                sprintf( players[i] -> buffer, "CASH %d %d\n", -cashOffset, players[i] -> GetCash() );
            }
            players[i] -> Send( [this, i](){ players[i] -> GameOver((Game*)this); } );
        }
        sprintf( players[minPlayerInd] -> buffer, "CASH %d %d\n", winnerGet , players[minPlayerInd] -> GetCash() );
        players[minPlayerInd] -> Send( [this, minPlayerInd](){ players[minPlayerInd] -> GameOver((Game*)this); } );

    }
        
}
                
    
GuessNumber::~GuessNumber(){
}
