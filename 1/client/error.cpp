#include <cstdio>
#include <cstdlib>

void error( const char*strErr ){
    perror( strErr );
    exit(1);
}

void warn( const char*strWarn ){
    perror( strWarn );
};
