#ifndef SERVER_HPP
#include "server.hpp"
#endif
#include <map>
// #include "player.hpp"
#include "black_jack.hpp"
#include "guess_number.hpp"

#define INITIAL_CASH 10000
#define ROOM_NUMBER 10 

class GameSystem{
public:
    GameSystem(Server&svr);
    void NewClient(int);
    
private:
    Server&server;
    std::map<int,BlackJack*>bjRooms;
    std::map<int,GuessNumber*>gnRooms;
    //char roomStatus[256];
    void UpdateRoomStatus(char*);
    void SelectGame(Player*);
    int maxRoomId;
};


