#define CLIENT_HPP

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "error.h"

#define BUFFER_SIZE 256


class Client{
public:
    Client(char* ip, int port);
    char* ReadLine();
    void Write();
    char buffer[BUFFER_SIZE];
private:
    void Read();
    int socketFd;
    char readBuffer[BUFFER_SIZE];
    char* readBufferStart;
};
