#include "guess_number.hpp"
#include <string.h>
#include <iostream>

using namespace std;

void ReadGuess(char*buffer){
    int nums[4];
    int appear[10] = {0};
    for( int i = 0 ; i < 10 ; ++i ){
        appear[i] = 0;
    }
    for( int i = 0 ; i < 4 ;++i ){
        cin >> nums[i];        
    }
    for( int i = 0 ; i < 4 ; ++i ){
        if( nums[i] < 0 || nums[i] > 9 ){
            cout << "Each number is in [0,9]. Please input again." << endl;
            ReadGuess(buffer);
            return;
        }
        if( appear[nums[i]] ){
            cout << "Each number only appears once. Please input again." << endl;
            ReadGuess(buffer);
            return;
        }
        appear[nums[i]] = 1;
    }
    sprintf( buffer, "%d %d %d %d\n",
             nums[0], nums[1], nums[2], nums[3] );
}
    
static int Play(Client&client){
    // Read START
    char*res = client.ReadLine();

    cout << "Game start!" << endl
         << "Please input you guess!" << endl;

    ReadGuess( client.buffer );
    client.Write();
    res = client.ReadLine();

    char head[40];
    sscanf( res, "%s", head );

    int round = 2;
    while( strcmp( head, "TRY_AGAIN" ) == 0 ){
        int a, b;
        sscanf( res, "%s %d %d", head, &a, &b );    
        cout << a << "A " << b << "B. Try again! (Round " << round++ << " )" << endl;
        ReadGuess( client.buffer );
        client.Write();
        res = client.ReadLine();
        sscanf( res, "%s", head );
    }
    if( strcmp( head, "FAIL" ) == 0 ){
        cout << "Sorry, you fail" << endl;
        res = client.ReadLine();
        int ans[4];
        sscanf( res, "%d %d %d %d",
                &ans[0], &ans[1], &ans[2], &ans[3] );
        cout << "Answer is: "
             << ans[0] << " "
             << ans[1] << " "
             << ans[2] << " "
             << ans[3] << endl;
    }
    else{
        cout << "Congratulate! You win!" << endl;
    }

    // Read cash line
    res = client.ReadLine();

    int cashOffset;
    sscanf( res, "%s %d", head, &cashOffset );
    if( cashOffset < 0 ){
        cout << "You lose $" << -cashOffset << endl;
    }
    else{
        cout << "You win $" << cashOffset << endl;
    }
    cout << "Enter to continue." << endl;
    cin.get();
    cin.ignore();
    return cashOffset;
}    

int GuessNumber(Client&client, int roomId){
    cout << "\033[2J" << endl;
    cout << "\033[1;1H" << endl;

    sprintf(client.buffer, "2 %d\n", roomId );

    // Write send choose room id.
    client.Write();

    // Read WAIT
    char*res = client.ReadLine();
    char head[30];
    sscanf( res, "%s", head );
    
    // Check if room available
    if( strcmp( head, "WAITING") != 0 ){
        cout << "Sorry, the room is not available now." << endl
             << "Enter anykey to continue." << endl;
        cin.get();
        cin.ignore();
        return 0;
    }

    cout << "Enter anything to become ready!" << endl;
    cin.get();
    cin.ignore();
    // Send start signal.
    client.Write();
    
    return Play(client);
}

int GuessNumber(Client&client){
    cout << "\033[2J" << endl;
    cout << "\033[1;1H" << endl;

    strcpy(client.buffer, "0\n" );

    // Send choose
    client.Write();

    // Read WAIT
    client.ReadLine();

    // Send Start signal
    client.Write();

    return Play(client);
}
