#include <functional>
#include <iostream>
#include "game_system.hpp"
#include "error.h"


GameSystem::GameSystem(Server&svr): server(svr) {
    for( int i = 0 ; i < ROOM_NUMBER ; ++i ){
        auto*gn = new GuessNumber([this](Game*game){ delete game; });
        std::cerr << gn << std::endl;
        gnRooms[i] = gn;
        bjRooms[i] = new BlackJack([this](Game*game){delete game; });
    }
    maxRoomId = 2 * ROOM_NUMBER ;
}

void GameSystem::NewClient(int fd){
#ifdef DEBUG
    fprintf( stderr, "Hi there is new client with fd = %d \n ", fd );
#endif
    std::cerr << "game system pty = " << this << std::endl;    
    Player*player = new Player( server, fd, INITIAL_CASH );
    std::function<void (Game*)>gameOverCallback = [this, player](Game* game){
        std::cerr << "game system ptr in gameOverCallback = " << this << std::endl;
        if( game -> GetType() == Game::Type::GuessNumber && gnRooms.count(game -> GetId()) ){
            this -> gnRooms.erase( game -> GetId() );
            GuessNumber* newGame = new GuessNumber([this](Game*game){ delete game; }) ;
            gnRooms[newGame -> GetId()] = newGame;
        }
        else if( bjRooms.count(game -> GetId() ) ){
            this -> bjRooms.erase( game -> GetId() );
            BlackJack* newGame = new BlackJack([this](Game*game){ delete game; }) ;
            bjRooms[newGame -> GetId()] = newGame;
        }
        UpdateRoomStatus(player -> buffer);
        player -> Send( [this, player](){ SelectGame(player); });
    };
    player -> OnGameOver( gameOverCallback );

    UpdateRoomStatus(player -> buffer);
    player -> Send( [this, player](){ SelectGame(player); });
}


void GameSystem::SelectGame(Player*player){
    std::function<void (int)> onDestroy = [](int fd){
#ifdef DEBUG
        std::cerr << "Game system client with fd = " << fd << " leave." << std::endl;
#endif
    };
    player -> Receive( [this, player](){

            int gameType;
            sscanf( player -> buffer, "%d", &gameType );
#ifdef DEBUG
            fprintf( stderr, "Player with fd =  choose game %d.\n", gameType );
#endif

            std::function<void (Game*)>destroyCallback = [this](Game*game ){
                delete game;
            };
            
            switch( gameType ){
            case 0:{
                new GuessNumber( player, destroyCallback );
                break;
            }
            case 1:
                new BlackJack( player, destroyCallback) ;
                break;
            case 2:{
                int roomId = 0;
                sscanf( player -> buffer, "%d %d", &gameType, &roomId);
#ifdef DEBUG
                std::cerr << "Player choose room " << roomId << std::endl;
#endif

                if( gnRooms.count(roomId) == 0 ||
                    gnRooms[roomId] -> AddPlayer( player ) < 0){
                    UpdateRoomStatus(player -> buffer);
                    player -> Send( [this, player](){ SelectGame(player); });
                }
                break;
            }
            case 3:{
                int roomId;
                sscanf( player -> buffer, "%d %d", &gameType, &roomId);
#ifdef DEBUG
                std::cerr << "Player choose room " << roomId << std::endl;
#endif
                if( bjRooms.count(roomId) == 0 ||
                    bjRooms[roomId] -> AddPlayer( player ) < 0){
                    UpdateRoomStatus(player -> buffer);
                    player -> Send( [this, player](){ SelectGame(player); });
                }
                break;
            }
            default:
                warn("Invalid game selection. Someone is annoying!!\n");
            }
        });
}


void GameSystem::UpdateRoomStatus(char* buffer){
    strcpy( buffer, "ROOM_STATUS " );
    for( auto it = gnRooms.begin() ; it != gnRooms.end() ; ++ it ){
        sprintf( buffer + strlen( buffer ),
                 "%d %d|", it -> first, (it -> second) -> GetNPlayers() );
    }
    
    for( auto it = bjRooms.begin() ; it != bjRooms.end() ; ++ it ){
        sprintf( buffer + strlen( buffer ),
                 "%d %d|", it -> first, (it -> second) -> GetNPlayers() );
    }
    sprintf( buffer + strlen( buffer ), "\n");

    
#ifdef DEBUG
    // std::cerr << "Room Status: " << buffer << std::endl;
#endif
}
