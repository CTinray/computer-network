#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <functional>
#ifndef IO_HPP
#include "io.hpp"
#endif
#define SERVER_HPP
#define MAX_FDS 1024
#define SEL_TIMEOUT 10

class Server{

public:

    Server(int);
    void read(int, char*buffer, int bufferSize,
              std::function<void (int)>onFinish, std::function<void (int)>onDestroy);
    void write(int, char*buffer, int bufferSize,
               std::function<void (int)>onFinish, std::function<void (int)>onDestroy);
    void  StartIOLoop();
    void onAccept(std::function<void (int)> onAcceptCallback );
    void close(int);
private:
    int  listenFD;    
    std::function<void (int)>* onAcceptCallback;
    IO ios[MAX_FDS];
    fd_set readFDSet, readFDSetCpy;
    fd_set writeFDSet, writeFDSetCpy;
    fd_set exceptionFDSet, excFDSetCpy;
    bool enableIOLoop;
    int maxFDs;
    void CheckIOStatus();
};
