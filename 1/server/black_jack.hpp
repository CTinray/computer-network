#ifndef SERVER_HPP
#include "server.hpp"
#endif
#include <vector>
#include <functional>
#ifndef GAME_HPP
#include "game.hpp"
#endif

class BlackJack: public Game{
public:
    BlackJack(std::function<void (Game*)>);
    BlackJack(Player*,std::function<void (Game*)>);
    virtual int AddPlayer(Player*);
    virtual Game::Type GetType(){ return Game::Type::BlackJack; };
    virtual int GetId(){ return id; }
    virtual ~BlackJack();
private:
    int id;
 static int counter;
    class Card{
    public:
        enum Suit{ Club=1, Spad=2, Diamond=3, Heart=4 };
        Card( int rank, Card::Suit suit ){ this -> rank = rank; this -> suit = suit; }
        int rank;
        bool operator <( Card&b){ return this -> rank < b.rank; }
        bool operator >( Card&b){ return this -> rank > b.rank; }
        Suit suit;
        int GetPoint(){ return rank >= 10 ? 10 : rank; };
        int GetCode(){ return suit * 100 + rank; }
    };
    enum DeckStatus{ DeckBlackJack, DeckTwentyOne, DeckBust, DeckNothing };
    DeckStatus CheckDeckStatus(std::vector<Card>&);
    
    // class CardPlayer{
    // public:
    //     CardPlayer
    void InitDeck();
    void ShuffleDeck();
    Card GetCard();
    void WaitReady(Player*);
    void DealFirstCard();
    void CheckStatus(int, int);
    void HandleRequest(int, int);
    void DealerHit();
    void DispachMoney();
    void BroadCastDeck(std::vector<Card>&);
    void Broadcast(char*, std::function<void (void)>);
    void Terminate(int, int );
    enum Request{ Hit=0, Stand=1, Split=2 };
    std::function<void (Game*)>destroyCallback;
    std::vector<Card>deck;
    std::vector<Card> dealerDeck;
    std::vector<std::vector<std::vector<Card>>>playerDecks;
    std::vector<Player*>players;
    char buffer[256] = "";
    int nPlayerReady = 0;
    int DeckMaxSum( std::vector<Card>& );
    bool terminateGame;
};
    
