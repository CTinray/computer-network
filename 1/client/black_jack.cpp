#include "black_jack.hpp"
#include <iostream>
#include <vector>
using namespace std;

void ReadRequest(char*buffer, vector<int>&deck){
    bool split = (deck.size() == 2 && deck[0]%100 == deck[1]%100 );
    cout << "Do you want to" << endl
         << "(1) Hit" << endl
         << "(2) Stand" << endl;
    if( split ){
        cout << "(3) Split" << endl;
    }
    int choise;
    cin >> choise;
    if( choise < 0 || ( choise > 2 && !split )
        || ( choise > 3 && split ) ){
        cout << "Your choise is invalid." << endl;
        ReadRequest( buffer, deck );
    }
    sprintf( buffer, "%d\n", choise - 1);
}

void PrintCard(int card ){
    if( card == -1 )
        cout << "□ ";
    else{
        const char* suits[4] = { "♣ ", "♠ ", "\e[95m♦ ", "\e[95m♥ " };
        cout << suits[card / 100];
        cout << card % 100 << "\e[39m ";
    }
}

void PrintCards(int playerInd, vector<int>&dealerDeck, vector<vector<vector<int>>>&playerCards){
    cout << "\033[2J\033[1;1H";
    cout << "Dealer's cards: ";
    for( auto it = dealerDeck.begin() ; it != dealerDeck.end() ; ++it ){
        PrintCard( *it );
        cout << " ";
    }
    cout << endl;
    int counter = 1;
    for( auto player = playerCards.begin();
         player != playerCards.end(); ++ player ){
        if( counter == playerInd + 1 ){
            cout << "\e[100m";
        }
        for( auto deck = player -> begin();
             deck != player -> end(); ++ deck ){
            cout << "P" << counter << ": ";
            for( auto card = deck -> begin();
                 card != deck -> end() ; ++ card ){
                PrintCard( *card );
            }
            cout << endl;
        }
        cout << "\e[49m";
        ++counter;
    }
    cout << endl;
}

static int Play(Client&client){
    // Read START
    client.ReadLine();
    cout << "Game start!" << endl;
    
    char head[40];
    int playerInd, nPlayers;
    vector<vector<vector<int>>>playerCards;
    vector<int>dealerCards;
    
    // Read YOU_ARE
    char*res = client.ReadLine();
    sscanf( res, "%s %d %d", head, &playerInd, &nPlayers );
    cout << "You are P" << playerInd << endl;

    playerCards.resize( nPlayers );
    
    // Read CARDS
    int card, nRead;
    res = client.ReadLine();
    sscanf( res, "%s %d%n", head, &card, &nRead);
    res += nRead;
    dealerCards.push_back( -1 );
    dealerCards.push_back( card );
    for( auto playerDecks = playerCards.begin() ;
         playerDecks != playerCards.end(); ++ playerDecks ){
        playerDecks -> resize(1);
        sscanf( res, "%d%n", &card, &nRead);
        res += nRead;
        (*playerDecks)[0].push_back( -1 );
        (*playerDecks)[0].push_back( card );        
    }

    // Read HIDE_CARD
    res = client.ReadLine();
    sscanf( res, "%s %d", head, &card );
    playerCards[playerInd][0][0] = card;

    PrintCards( playerInd, dealerCards, playerCards );
    
    for( unsigned int p = 0 ; p < playerCards.size() ; ++p ){
        auto*player = &(playerCards[p]);
        for( unsigned int i = 0 ; i < player -> size() ; ++i ){            
            res = client.ReadLine();
            bool yourTurn = false;
            sscanf( res, "%s", head );
            if( playerInd == p ){
                yourTurn = true;
            }            
            while( strcmp( head, "DECK_CONTINUE" ) == 0 ){
                if( yourTurn ){
                    ReadRequest( client.buffer, (*player)[i] );
                    client.Write();
                }
                res = client.ReadLine();
                sscanf( res, "%s%n", head, &nRead );
                res += nRead;
                if( strcmp( head, "CARD" ) == 0 ){
                    sscanf( res, "%d", &card );                        
                    (*player)[i].push_back( card );
                }
                else if( strcmp( head, "SPLIT" ) == 0 ){
                    int lastCard = (*player)[i].back();
                    (*player)[i].pop_back();                                        
                    player -> resize( player -> size() + 1);
                    vector<int>&lastDeck = player -> back();
                    lastDeck.push_back( lastCard );
                }
                PrintCards( playerInd, dealerCards, playerCards );
                res = client.ReadLine();
                sscanf( res, "%s", head );
            }
        }
    }

    // Read dealer cards
    res = client.ReadLine();
    int nCards;
    sscanf( res, "%s %d%n", head, &nCards, &nRead );
    res += nRead;
    dealerCards.clear();
    for( int i = 0 ; i < nCards ; ++i ){        
        sscanf( res, "%d%n", &card, &nRead );
        res += nRead;
        dealerCards.push_back( card );
    }
    PrintCards( playerInd, dealerCards, playerCards );

    // Get something like DEALER_BUST
    res = client.ReadLine();
    sscanf( res, "%s", head );

    // Get cash modify
    res = client.ReadLine();

    int cashOffset;
    sscanf( res, "%s %d", head, &cashOffset );
    if( cashOffset < 0 ){
        cout << "You lose $" << -cashOffset << endl;
    }
    else{
        cout << "You win $" << cashOffset << endl;
    }
    cout << "Enter to continue." << endl;
    cin.get();
    cin.ignore();
    return cashOffset;
}
    
int BlackJack(Client&client){
    cout << "\033[2J" << endl;
    cout << "\033[1;1H" << endl;
    strcpy(client.buffer, "1\n" );

    // Send choose
    client.Write();

    // Read WAIT
    client.ReadLine();

    // Send Start signal
    client.Write();

    return Play(client);    
}

    
int BlackJack(Client&client, int roomId){
    cout << "\033[2J\033[1;1H" << endl;
    sprintf(client.buffer, "3 %d\n", roomId );

    // Write send choose room id.
    client.Write();

    // Read WAIT
    char*res = client.ReadLine();
    char head[40];
    sscanf( res, "%s", head );
    
    // Check if room available
    if( strcmp( head, "WAITING") != 0 ){
        cout << "Sorry, the room is not available now." << endl
             << "Enter anykey to continue." << endl;
        cin.ignore();
        return 0;
    }

    cout << "Enter anything to become ready!" << endl;
    cin.get();
    cin.ignore();

    // Send start signal.
    client.Write();

    return Play(client);
}
