#include "client.hpp"

Client::Client(char* ip, int port ){
    // Open the socket.
    socketFd = socket( AF_INET, SOCK_STREAM, 0);
	if( socketFd < 0){
		error("Error: fail to open socket.");
	}
    struct sockaddr_in serverAddress;
	// Initialize the connection.
	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	inet_aton( ip , &serverAddress.sin_addr );
	serverAddress.sin_port = htons( port );
	if( connect( socketFd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0){
		error("Error: fail to connect to server");
	}
    readBuffer[0] = '\0';
    readBufferStart = readBuffer;
}


void Client::Read(){
    int bufferSize = sizeof( readBuffer );
    char*bufferStart = readBuffer + strlen( readBuffer );
    for( char*it = bufferStart; it < readBuffer + sizeof(readBuffer) ; ++it ){
        *it = '\0';
    }
    while( strstr( readBuffer, "\n" ) == NULL ){
        int nRead = read( socketFd, bufferStart, readBuffer + bufferSize - bufferStart );
        if( nRead < 0 ){
            char strWarn[100];
            sprintf( strWarn, "Warning: fail to read" );
            warn( strWarn );        
        }                
        bufferStart += nRead;
    }    
}

char* Client::ReadLine(){
    char*newLine;
    if( ( newLine = strchr( readBufferStart, '\n' )) != NULL ){
        *newLine = '\0';
        char*origin = readBufferStart;
        readBufferStart = newLine + 1;
        return origin;
    }
    else{
        
        for( unsigned int i = 0 ; i <= strlen( readBufferStart ); ++i ){
            readBuffer[i] = *(readBufferStart + i);
        }
        Read();

        readBufferStart = readBuffer;
        return ReadLine();
    }
}

void Client::Write(){
    char*bufferStart = buffer;
    char*bufferEnd = buffer + strlen( buffer );
    while( bufferStart != bufferEnd ){
        int written = write( socketFd, bufferStart, strlen( bufferStart ) );
        bufferStart += written;
    }
}
