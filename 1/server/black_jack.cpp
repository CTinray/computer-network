#include <algorithm>
#include <cstring>
#include "black_jack.hpp"
#include <iostream>


int BlackJack::counter = 0;


BlackJack::BlackJack(Player*player,std::function<void (Game*)>onDestroy)
    : BlackJack(onDestroy){
    this -> AddPlayer( player );
}

BlackJack::BlackJack(std::function<void (Game*)>onDestroy)
    : destroyCallback(onDestroy), terminateGame(false){
    id = counter ++;
    nPlayers = 0;
    InitDeck();
    ShuffleDeck();
}

int BlackJack::AddPlayer(Player*player){
    if( nPlayers >= 4 ){
        return -1;
    }
    nPlayers += 1;
    players.push_back( player );
    playerDecks.resize( nPlayers );
    strcpy( player -> buffer , "WAITING\n" );
    player -> Send( [this, player](){
            WaitReady(player);
        });
    player -> OnError([this](int){
            for( auto it = players.begin();
                 it != players.end(); ++it ){
                (*it) -> OnError([](int){});
            }
#ifdef GAME_LOG
            fprintf(stderr, "Terminate black jack\n");
#endif
            terminateGame = true;
        });
    return 0;
}

void BlackJack::WaitReady(Player*player){
    player -> Receive( [this](){
            nPlayerReady += 1;
            if( nPlayerReady == nPlayers ){
                status = Status::Playing;
                DealFirstCard();
            }
        });    
}


void BlackJack::InitDeck(){    
    for( int suit = 0 ; suit < 4 ; ++ suit ){
        for( int rank = 1 ; rank <= 13 ; ++rank ){
            deck.push_back( Card( rank , (Card::Suit)suit ) );
        }        
    }
}

void BlackJack::ShuffleDeck(){
    std::random_shuffle( deck.begin(), deck.end() );    
#ifdef DEBUG
    deck.push_back( Card( 2, Card::Suit::Spad ) );
    deck.push_back( Card( 2, Card::Suit::Diamond ) );
    deck.push_back( Card( 3, Card::Suit::Spad ) );
    deck.push_back( Card( 3, Card::Suit::Diamond ) );
#endif
}

BlackJack::Card BlackJack::GetCard(){
    Card card = deck.back();
    deck.pop_back();
    return card;
}

#define CONTINUE 1
#define END_OF_DECK 0
void BlackJack::DealFirstCard(){
    dealerDeck.push_back( GetCard() );
    dealerDeck.push_back( GetCard() );
    for( int i = 0 ; i < nPlayers; ++i ){
        playerDecks[i].resize(1);
        playerDecks[i][0].push_back( GetCard() );
        playerDecks[i][0].push_back( GetCard() );
    }
    char cards[100];

    sprintf( cards, "%d ", dealerDeck[1].GetCode() );
    for( int i = 0 ; i < nPlayers ; ++i ){
        sprintf( cards + strlen( cards ), "%d ", playerDecks[i][0][1].GetCode() );
    }
    nPlayerReady = 0;
    for( int i = 0 ; i < nPlayers ; ++i ){
        sprintf( players[i] -> buffer, "START\nYOU_ARE %d %d PLAYERS\n CARDS %s\nHIDE_CARD %d\n", i, nPlayers, cards, playerDecks[i][0][0].GetCode() );
        players[i] -> Send( [this](){
                nPlayerReady += 1;
                if( nPlayerReady == nPlayers ){
                    // sprintf( players[0] -> buffer, "YOUR_TURN\n" );
                    // players[0] -> Send( [this](){ 
                    CheckStatus( 0, 0 );
                    // });
                }
            });
    }
    
}

void BlackJack::Terminate(int playerInd, int deckInd ){
    if( playerInd < nPlayers ){
        if( deckInd < playerDecks[playerInd].size() ){
            strcpy( buffer, "STAND\nEND_OF_DECK" );
            Broadcast( buffer, [this, playerInd, deckInd](){
                    Terminate( playerInd, deckInd + 1 );
                });
        }
        else{
            Terminate( playerInd + 1, deckInd);
        }
    }
    else{
        sprintf( buffer, "END_OF_GAME \nCASH 0 0\n");
        Broadcast( buffer, [this](){
                destroyCallback(this);
            });
    }    
}    

void BlackJack::CheckStatus(int playerInd, int deckInd ){
    if( terminateGame ){
        Terminate(playerInd, deckInd);
    }
    if( deckInd >= playerDecks[playerInd].size() ){
        if( playerInd < nPlayers - 1 ){
            // sprintf( players[playerInd + 1] -> buffer, "YOUR_TRUN\n" );
            // players[playerInd + 1] -> Send( [this, playerInd](){
            CheckStatus( playerInd + 1, 0 );
            // } );
            return;
        }else{
#ifdef GAME_LOG
            std::cerr << "Start to deal with the dealer" << std::endl;
#endif             
            DealerHit();
            return;
        }
    }
    auto&deck = playerDecks[playerInd][deckInd];
    DeckStatus status = CheckDeckStatus( deck );
    if( status != DeckStatus::DeckNothing){
        strcpy( buffer, "DECK_END\n" );
        Broadcast( buffer, [this, playerInd, deckInd](){
                CheckStatus(playerInd, deckInd + 1);        
            });
    }
    else if( status == DeckStatus::DeckNothing ){
        strcpy( buffer, "DECK_CONTINUE\n" );        
        Broadcast( buffer,[this, playerInd, deckInd](){
                players[playerInd] -> Receive( [this, playerInd, deckInd](){
                        HandleRequest(playerInd, deckInd );
                    });
            });
    }
    return;
}

void BlackJack::HandleRequest(int playerInd, int deckInd){
    // if( deckInd >= nPlayerDeck ){
    //     this -> onFinish(this -> fd);
    //     return;
    // }
    int req;
    Player*player = players[playerInd];
    auto& playerDeck = playerDecks[playerInd][deckInd];
    sscanf( player -> buffer, "%d", &req );
    
#ifdef GAME_LOG
    std::cerr << "req = " << req ;
    switch( req ){
    case Request::Hit:
        std::cerr << "Player " << playerInd << " hit." << std::endl;
        break;
    case Request::Stand:
        std::cerr << "Player " << playerInd << " stand." << std::endl;
        break;
    case Request::Split:
        std::cerr << "Player " << playerInd << " split." << std::endl;
        break;
    }
#endif
    
    switch( req ){
    case Request::Hit:{
        Card card = GetCard();
        playerDeck.push_back( card );
        sprintf( buffer, "CARD %d\n", card.GetCode() );
        Broadcast( buffer, [this, playerInd, deckInd](){
                CheckStatus( playerInd, deckInd );
            });
        return;
    }
    case Request::Stand:
        strcpy( buffer, "STAND\nEND_OF_DECK\n" );
        Broadcast( buffer, [this, playerInd, deckInd](){
                CheckStatus( playerInd, deckInd + 1 );
            });
        return;
    case Request::Split:
        if( playerDeck.size() == 2 &&
            playerDeck[0].rank == playerDeck[1].rank &&
            playerDecks[playerInd].size() < 4){
            int newNDecks = playerDecks[playerInd].size() + 1;
            playerDecks[playerInd].resize( newNDecks );
            playerDecks[playerInd][newNDecks - 1].push_back( playerDecks[playerInd][deckInd][1] );
            playerDecks[playerInd][deckInd].pop_back();
            Broadcast( (char*)"SPLIT\n", [this, playerInd, deckInd](){
                    CheckStatus( playerInd, deckInd );
                });

        }else{
#ifdef DEBUG
            fprintf( stderr, "BlackJack player with ind = %d request invalid split. Abort.\n", playerInd );
#endif
            player -> Receive([this, playerInd, deckInd](){ HandleRequest( playerInd, deckInd ); } );
        }
    }
}

void BlackJack::DealerHit(){
    int sum = dealerDeck[0].GetPoint() + dealerDeck[1].GetPoint();
    DeckStatus deckStatus = CheckDeckStatus( dealerDeck );
    while( sum < 17 && deckStatus == DeckStatus::DeckNothing ){
        Card card = GetCard();
        sum += card.GetPoint();
        dealerDeck.push_back( card );
        deckStatus = CheckDeckStatus( dealerDeck );
    }
    sprintf( buffer, "DEALER_CARDS %d ", (int)dealerDeck.size());
    for( unsigned int i = 0 ; i < dealerDeck.size() ; ++i ){
        sprintf( buffer + strlen(buffer), "%d ", dealerDeck[i].GetCode() );
    }
    sprintf( buffer + strlen(buffer), "\n" );
    Broadcast( buffer, [this](){ DispachMoney(); } );
}


BlackJack::DeckStatus BlackJack::CheckDeckStatus(std::vector<BlackJack::Card>&deck){
    std::sort( deck.begin(), deck.end() );
    if( deck.size() == 2 && deck[0].rank == 1 && deck[0].suit == Card::Suit::Spad && deck[1].rank == 11 &&
        ( deck[1].suit == Card::Suit::Spad || deck[1].suit == Card::Suit::Club )) {
        return DeckStatus::DeckBlackJack;
    }

    if( deck[0].rank == 1 ){
        // Take the 1 as 11
        int sum = 11;
        for( int i = 1 ; i < deck.size() ; ++i ){
            sum += deck[i].GetPoint();
        }
        if( sum == 21 ){ return DeckStatus::DeckTwentyOne; }        
    }

    int sum = 0;
    for( auto it = deck.begin(); it != deck.end() ; it++ ){
        sum += (*it).GetPoint();
    }
    if( sum == 21 ){
        return DeckStatus::DeckTwentyOne;
    }
    if( sum > 21 ){
        return DeckStatus::DeckBust;
    }
    return DeckStatus::DeckNothing;
}

int BlackJack::DeckMaxSum( std::vector<Card>&deck ){
    std::sort( deck.begin(), deck.end() );
    int sum = 0;
    if( deck[0].rank == 1 ){
        sum += 11;
    }
    for( int i = 1 ; i < deck.size() ; ++i ){
        sum += deck[i].GetPoint();
    }
    if( sum <= 21 ){
        return sum;
    }
    sum = 0;
    for( int i = 0 ; i < deck.size() ; ++i ){
        sum += deck[i].GetPoint();
    }
    return sum;
}

#define BET 1000

void BlackJack::DispachMoney(){
#ifdef DEBUG
    std::cerr << "Start to dispach money." << std::endl;
#endif
    auto dealerDeckStatus = CheckDeckStatus( dealerDeck );
    const char* strYouLose = "YOU_LOSE";
    const char* strBlackJack = "YOU_BLACK_JACK";
    const char* strYouFive = "YOU_FIVE";
    const char* strYouSeven = "YOU_SEVEN";
    const char* strYouWin = "YOU_Win";
    const char* strYouStraight = "YOU_STRAIGHT";
    const char* strYouOneTen = "YOU_ONE_TEN";
    const char* strDealerBust = "DEALER_BUST";
    const char*strStatus = "TIE";
    for( int i = 0 ; i < nPlayers ; ++i ){
        auto player = players[i];
        int moneyShift = 0;
        for( unsigned int j = 0 ; j < playerDecks[i].size() ; ++ j ){
            auto deck = playerDecks[i][j];
            auto status = CheckDeckStatus( deck );
            if( status == DeckStatus::DeckBust ||
                ( DeckMaxSum( deck ) < DeckMaxSum( dealerDeck ) &&
                  dealerDeckStatus != DeckStatus::DeckBust )){
                moneyShift -= BET;
                strStatus = strYouLose;
            }
            else if( status == DeckStatus::DeckBlackJack ){
                moneyShift += BET * 5;
                strStatus = strBlackJack;
            }
            else if ( deck.size() >= 5 ){
                moneyShift += BET * 3;
                strStatus = strYouFive;
            }
            else if ( deck.size() == 3 &&
                      deck[0].rank == 7 &&
                      deck[1].rank == 7 &&
                      deck[2].rank == 7 ){
                moneyShift += BET * 10;
                strStatus = strYouSeven;
            }
            else if ( deck.size() == 3 &&
                      deck[0].rank == 6 &&
                      deck[1].rank == 7 &&
                      deck[2].rank == 8 ){
                moneyShift += BET * 15;
                strStatus = strYouStraight;
            }
            else if ( deck.size() == 2 &&
                      deck[0].rank == 1 &&
                      deck[1].rank == 11 ){
                moneyShift += BET * 1.5;
                strStatus = strYouOneTen;
            }
            else if( dealerDeckStatus == DeckStatus::DeckBust ){
                moneyShift += BET;
                strStatus = strDealerBust;
            }
            else if( DeckMaxSum( deck ) > DeckMaxSum( dealerDeck ) ){
                moneyShift += BET;
                strStatus = strYouWin;
            }
#ifdef GAME_LOG
            std::cerr << "Outcome: " << strStatus << std::endl ;
#endif        

        }

        player -> ModifyCash( moneyShift );
        sprintf( player -> buffer, "%s\nCASH %d %d\n", strStatus, moneyShift, player -> GetCash() );
        nPlayerReady = 0;
        player -> Send( [player, this](){
                player -> GameOver((Game*)this);
                if( nPlayerReady == nPlayers ){
                    destroyCallback( this );
                }
            } );
    }
}
                
void BlackJack::Broadcast(char*buffer,  std::function<void (void)>cb ){
#ifdef DEBUG
    std::cerr << "Broadcast " << buffer << std::endl;
#endif
    nPlayerReady = 0;
    for( int i = 0 ; i < nPlayers ; ++i ){
        strcpy( players[i] -> buffer, buffer );
        players[ i ] -> Send( [this, cb](){
                nPlayerReady += 1;
                if( nPlayerReady == nPlayers ){
                    cb();
                }
            });
    }
}
            
 
BlackJack::~BlackJack(){
}
