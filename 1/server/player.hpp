#define PLAYER_HPP
#ifndef SERVER_HPP
#include "server.hpp"
#endif
#include <functional>

class Game;
#define BUFFER_SIZE 256

class Player{
public:
    Player( Server&sv, int, int );
    void Send(std::function<void ()>);
    void Receive(std::function<void ()>);
    void OnError(std::function<void (int)>);
    void ModifyCash(int);
    int Cash();
    int GetCash();
    void OnGameOver(std::function<void (Game*)>);
    void GameOver(Game*);
    void Destroy();
    friend class GameSystem;
    char buffer[BUFFER_SIZE];
private:
    int fd, cash;
    Server&server;
    std::function<void (int)>errorCallback;
    std::function<void (Game*)>gameOverCallback;
};
    
